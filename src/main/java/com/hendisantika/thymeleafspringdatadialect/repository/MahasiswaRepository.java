package com.hendisantika.thymeleafspringdatadialect.repository;

import com.hendisantika.thymeleafspringdatadialect.domain.Mahasiswa;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : thymeleaf-spring-data-dialect
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/03/20
 * Time: 09.14
 */
public interface MahasiswaRepository extends PagingAndSortingRepository<Mahasiswa, String> {
}
