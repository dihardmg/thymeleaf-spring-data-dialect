package com.hendisantika.thymeleafspringdatadialect.controller;

import com.hendisantika.thymeleafspringdatadialect.domain.Mahasiswa;
import com.hendisantika.thymeleafspringdatadialect.repository.MahasiswaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import org.springframework.data.web.PageableDefault;

/**
 * Created by IntelliJ IDEA.
 * Project : thymeleaf-spring-data-dialect
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/03/20
 * Time: 09.15
 */
@Controller
public class MahasiswaController {

    private final static Logger LOGGER = LoggerFactory.getLogger(MahasiswaController.class.getName());

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @GetMapping("/index")
    public ModelMap getAll(@PageableDefault (size = 10) Pageable pageable) {
        return new ModelMap().addAttribute("mahasiswas", mahasiswaRepository.findAll(pageable));
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/index";
    }

    @GetMapping("/mahasiswa/mahasiswa-form")
    public ModelMap tampilFormedit(@RequestParam(required = false, value = "id") Mahasiswa mahasiswa) {
        if (mahasiswa == null) {
            mahasiswa = new Mahasiswa();
        }
        return new ModelMap().addAttribute("mahasiswa", mahasiswa);
    }

    @PostMapping("/mahasiswa/form")
    public String editMahasiswa(@ModelAttribute @Valid Mahasiswa mahasiswa, BindingResult errors,
                                SessionStatus status) {
        LOGGER.info(mahasiswa.toString());
        LOGGER.info(errors.toString());
        LOGGER.info("" + errors.hasErrors());
        LOGGER.info("" + errors.hasGlobalErrors());
        if (errors.hasErrors())
            return "/mahasiswa/mahasiswa-form";
        try {
            mahasiswaRepository.save(mahasiswa);
            status.setComplete();
            return "redirect:/index";
        } catch (DataAccessException e) {
            errors.reject("error.object", e.getMessage());
            LOGGER.error(e.getMessage());
            return "/mahasiswa/mahasiswa-form";
        }
    }

    @GetMapping("/mahasiswa/mahasiswa-details")
    public ModelMap tampilFormDetail(@RequestParam(required = false, value = "id") Mahasiswa mahasiswa) {
        if (mahasiswa == null) {
            mahasiswa = new Mahasiswa();
        }
        return new ModelMap().addAttribute("mahasiswa", mahasiswa);
    }

    @PostMapping("/mahasiswa/hapus")
    public String hapusMahasiswa(@RequestParam(name = "id") String id) {
        mahasiswaRepository.deleteById(id);
        return "redirect:/index";
    }
}
